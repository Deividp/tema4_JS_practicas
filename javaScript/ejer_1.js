
alert('Empezamos');

function comprobarCorreo(correo){
    var posArroba, posPunto;
    //Comprobar si hay una arroba
    posArroba = correo.indexOf('@');
    if(posArroba==-1){
        return  false;
    }
    posPunto = correo.lastIndexOf('.');
    if(posPunto == -1 || posPunto < posArroba){
        return false;
    }
    return true;
}

function comprobarFormulario(){

    var nombreCompleto;
    var correo;
    var edad;
    nombreCompleto = document.getElementById('nombreCompleto').value;
    correo = document.getElementById('correo').value;
    edad = document.getElementById('edad').value;
    //En caso de ser un nombre con menos de seis caracteres o no tener almenos un espacio en blanco se devolvera false
    if(nombreCompleto.length < 6){
        window.alert('Nombre corto');
        return false;
    }

    for(let i = 0; i < nombreCompleto.length; i++){
        if(nombreCompleto.charAt(i) !== ' '){
            
        }else{
        window.alert('No has introduciro más de una palabra.');
        return false;
        }
    }
    // Si el correo no es valido la función devuelve fase
    if(!(comprobarCorreo(correo))){
        window.alert('Correo incorrecto');
        return false;

    }
    //Ahora comprobar la edad
    if(isNaN(edad)){
        window.alert('La edad tiene que ser en número');
        return false;
    }else{
        if(edad > 65 || edad < 18 || edad == ''){
            window.alert('La edad introducida no es valida.');
            return false;
        }
    }
        return true;
}